# Paro VS. SMI en España

## Objetivo

El objetivo de este análisis es comprender qué relación existe entre el Salario Mínimo Interprofesional y las principales métricas de un país: El IPC, el PIB y el índice de paro.

## Resultados

He utilizado las correlaciones de Pearson, Kendall y Spearman (las tres que soporta Pandas nativamente):

|              | Pearson | Kendall | Spearman |
| ------------ | ------- | ------- | -------- |
| SMI vs. PIB  | -0.6764 | -0.0768 | -0.0988  |
| SMI vs. IPC  | +0.7516 | +0.3573 | +0.5177  |
| SMI vs. Paro | +0.3527 | +0.3224 | +0.3853  |

Además he utilizado las maravillosas técnicas de "prueba y error" y "estimación a ojo" para calcular cuánto es el retraso temporal entre la causa (el cambio en SMI) y el efecto.

Los siguientes gráficos representan el SMI junto con los tres estadísticos con los que queremos comparar el SMI:

![SMI vs. PIB](images/SMIvPIB.png "SMI vs. PIB")
![SMI vs. IPC](images/SMIvIPC.png "SMI vs. IPC")
![SMI vs. Índice de Paro](images/SMIvParo.png "SMI vs. Índice de Paro")

## Conclusiones

- Hay una correlación negativa con el PIB (si sube el salario mínimo, baja la productividad del país **con un retraso de 1 año**)
- Hay una correlación positiva con el IPC (si sube el salario mínimo, sube la inflación **con un retraso de 4 años**)
- Hay una correlación positiva con el paro (si sube el salario mínimo, sube el índice de paro **con un retraso de 6 años**)

## Fuentes

- [Paro](https://ine.es/dyngs/INEbase/es/operacion.htm?c=Estadistica_C&cid=1254736176918&menu=ultiDatos&idp=1254735976595)
- [IPC](https://ine.es/dyngs/INEbase/es/operacion.htm?c=Estadistica_C&cid=1254736164439&menu=ultiDatos&idp=1254735576581)
- [SMI](https://www.mites.gob.es/estadisticas/bel/SMI/index.htm)
