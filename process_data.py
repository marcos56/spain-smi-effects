#!/usr/bin/python3

import pandas as pd
import utils
import matplotlib.pyplot as plt
import numpy as np

pib = pd.read_csv("data/PIB.csv", sep=";", decimal=',', thousands='.')
smi = pd.read_csv("data/smi.csv", sep=";", decimal=',',
                  thousands='.', names=["year_orig", "amount"],
                  parse_dates=["year_orig"])
ipc = pd.read_csv("data/ipc.csv", sep=";", decimal=',', thousands='.')
paro = pd.read_csv("data/paro.csv", sep=";", decimal=',',
                   thousands='.')

pib = pib.loc[(pib["Tipo de dato"] == "Datos ajustados de estacionalidad y calendario") &
              (pib["Agregados macroeconómicos: Nivel 1"] == "Producto interior bruto a precios de mercado") &
              (pib["Niveles y tasas"] == "Variación trimestral")]
pib.drop(columns=["Tipo de dato", "Agregados macroeconómicos: Nivel 1",
                  "Agregados macroeconómicos: Nivel 2", "Niveles y tasas"], inplace=True)

ipc = ipc.loc[(ipc["Grupos ECOICOP"] == "Índice general")
              & (ipc["Tipo de dato"] == "Variación anual")]
ipc.drop(columns=["Grupos ECOICOP", "Tipo de dato"], inplace=True)

paro = paro.loc[(paro["Sexo"] == "Ambos sexos") &
                (paro["Comunidades y Ciudades Autónomas"] == "Total Nacional") &
                (paro["Edad"] == "Total")]
paro.drop(
    columns=["Sexo", "Comunidades y Ciudades Autónomas", "Edad"], inplace=True)

pib.dropna(how="any", inplace=True)
smi.dropna(how="any", inplace=True)
ipc.dropna(how="any", inplace=True)
paro.dropna(how="any", inplace=True)

pib["Periodo"] = pib["Periodo"].apply(utils.parse_period_t)
ipc["Periodo"] = ipc["Periodo"].apply(utils.parse_period_m)
paro["Periodo"] = paro["Periodo"].apply(utils.parse_period_t)
paro["Total"] = paro["Total"].apply(utils.parse_num_comma)

smi["smi_change"] = 100.0 * \
    (smi["amount"] - smi["amount"].shift(1))/smi["amount"].shift(1)
smi.sort_values("year_orig", axis=0, inplace=True,
                ignore_index=True, ascending=False)

paro["paro_change"] = 100.0 * \
    (paro["Total"] - paro["Total"].shift(1))/paro["Total"].shift(1)


smi = smi.loc[(smi["year_orig"].dt.year >= pib["Periodo"].min().year)
              | (smi["year_orig"].dt.year <= pib["Periodo"].max().year)]
smi["year"] = smi["year_orig"].dt.year + 0.03

print("\t\tPearson\tKendall\tSpearman")
utils.pib_corr(smi=smi, pib=pib)
utils.ipc_corr(smi=smi, ipc=ipc)
utils.paro_corr(smi=smi, paro=paro)

ax = pib.plot(x="Periodo", y="Total", label="Variación del PIB",
              grid=True, title="SMI Vs. PIB")
ax.axhline(y=0, color='r', linestyle='-')
smi.plot(x="year_orig", y="smi_change", label="Variación del SMI", ax=ax)

ax2 = ipc.plot(x="Periodo", y="Total", label="Variación del IPC",
               grid=True, title="SMI Vs. IPC")
ax2.axhline(y=0, color='r', linestyle='-')
smi.plot(x="year_orig", y="smi_change", label="Variación del SMI", ax=ax2)

ax3 = paro.plot(x="Periodo", y="Total",
                label="Variación de la tasa de Paro", grid=True, title="SMI Vs. Índice de Paro")
ax3.axhline(y=0, color='r', linestyle='-')
smi.plot(x="year_orig", y="smi_change", label="Variación del SMI", ax=ax3)

ax.grid(which='minor', alpha=0.2)
ax.grid(which='major', alpha=0.5)
ax2.grid(which='minor', alpha=0.2)
ax2.grid(which='major', alpha=0.5)
ax3.grid(which='minor', alpha=0.2)
ax3.grid(which='major', alpha=0.5)
plt.show()
