import pandas as pd


# Function to map trimester to a month and parse the date
def parse_period_t(period):
    year, trimester = period.split('T')
    trimester_to_month = {
        '1': '03',
        '2': '06',
        '3': '09',
        '4': '12'
    }
    # Construct a date string with the first day of the corresponding trimester
    date_str = f"{year}-{trimester_to_month[trimester]}-01"
    return pd.to_datetime(date_str)


# Function to map trimester to a month and parse the date
def parse_period_m(period):
    year, month = period.split('M')
    # Construct a date string with the first day of the corresponding trimester
    date_str = f"{year}-{month}-01"
    return pd.to_datetime(date_str)


def parse_num_comma(num) -> float:
    return float(num.replace(".", "").replace(",", "."))


def pib_corr(smi, pib):
    tmp_smi = smi.copy()
    tmp_smi.drop(columns=["amount"], inplace=True)

    tmp_pib = pib.copy()
    tmp_pib["year"] = tmp_pib["Periodo"].dt.year + \
        tmp_pib["Periodo"].dt.month/100.0
    tmp_pib["pib_change"] = tmp_pib["Total"]
    tmp_pib.drop(columns=["Periodo", "Total"], inplace=True)

    # Tras ver las correlaciones, se ve que tiene un desfase de 1 año
    tmp = pd.merge(tmp_pib, tmp_smi, on="year")
    tmp["pib_change_shift1"] = tmp["pib_change"].shift(1)

    pearson = tmp["smi_change"].corr(
        tmp["pib_change_shift1"], method="pearson")
    kendall = tmp["smi_change"].corr(
        tmp["pib_change_shift1"], method="kendall")
    spearman = tmp["smi_change"].corr(
        tmp["pib_change_shift1"], method="spearman")

    print("SMI VS. PIB\t%+.4f\t%+.4f\t%+.4f\t\tMide la correlación entre el cambio año a año del SMI con el cambio año a año del PIB" %
          (pearson, kendall, spearman))


def ipc_corr(smi, ipc):
    tmp_smi = smi.copy()
    tmp_ipc = ipc.copy()
    tmp_ipc["year"] = tmp_ipc["Periodo"].dt.year + \
        tmp_ipc["Periodo"].dt.month/100.0
    tmp_ipc.drop(columns=["Periodo"], inplace=True)

    tmp = pd.merge(tmp_ipc, tmp_smi, on="year")

    # Tras ver las correlaciones, se ve que tiene un desfase de 4 años
    tmp["ipc_change_shift4"] = tmp["Total"].shift(4)

    pearson = tmp["smi_change"].corr(
        tmp["ipc_change_shift4"], method="pearson")
    kendall = tmp["smi_change"].corr(
        tmp["ipc_change_shift4"], method="kendall")
    spearman = tmp["smi_change"].corr(
        tmp["ipc_change_shift4"], method="spearman")

    print("SMI VS. IPC\t%+.4f\t%+.4f\t%+.4f\t\tMide la correlación entre el cambio año a año del SMI con el cambio año a año del IPC" %
          (pearson, kendall, spearman))


def paro_corr(smi, paro):
    tmp_smi = smi.copy()
    tmp_paro = paro.copy()
    tmp_paro["year"] = tmp_paro["Periodo"].dt.year + \
        tmp_paro["Periodo"].dt.month/100.0

    tmp_paro.drop(columns=["Periodo"], inplace=True)

    tmp = pd.merge(tmp_paro, tmp_smi, on="year")
    tmp["paro_shift3"] = tmp["Total"].shift(6)

    pearson = tmp["smi_change"].corr(
        tmp["paro_shift3"], method="pearson")
    kendall = tmp["smi_change"].corr(
        tmp["paro_shift3"], method="kendall")
    spearman = tmp["smi_change"].corr(
        tmp["paro_shift3"], method="spearman")

    print("SMI VS. Paro\t%+.4f\t%+.4f\t%+.4f\t\tMide la correlación entre el cambio año a año del SMI con el cambio año a año del índice de paro" %
          (pearson, kendall, spearman))
